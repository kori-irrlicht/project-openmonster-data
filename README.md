# Project OpenMonster Test Client - Data

The data package to be used with the "Project OpenMonster" server.

This data package is licensed under the [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).

[Download](https://gitlab.com/kori-irrlicht/project-openmonster-test-client-data/-/archive/master/project-openmonster-test-client-data-master.tar.gz) the package 
and point the server to the download location by setting the "data.archive.location" to the download location and "data.archive.type" to "tgz"